# Elasticsearch Java (Spring Boot) Client Application
### General
This is a simple Java Spring Boot application that uses both the 
[Java Low Level REST Client](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-low.html)
and 
[Java High Level REST Client](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high.html)  
(both managed and developed by the Elasticsearch team), to communicate to an existing Elasticsearch Cluster.  

For this project, we are using IntelliJ and Maven

`Note`  
The High Level REST Client adds some abstractions over the Low Level REST Client, and makes building queries and communicating with the ES Cluster easier.  
However, if you want to have more control over these operations, or maybe construct the queries in the native 
[Query DSL](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html), than you can use the latter one.  
In this demo app we use both simply as a demostration.

`Keep in mind` There are other ways to communicate to Elasticsearch from Java application.
- [Jest](https://github.com/searchbox-io/Jest/tree/master/jest) is another third-party REST Client
- [Transport Client](https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/transport-client.html) is Java native client that communicates with ES
through it's native transport protocol, over tcp   
(will be deprecated in ES 7.0 and eventually removed in ES 8.0)
- [Spring Data Elasticsearch](https://projects.spring.io/spring-data-elasticsearch/) offers similar capabilities and abstractions as Spring Data JPA, but for Elastichsearch Documents:
    -  POJO centric model for interacting with a Elastichsearch Documents 
    -  ElasticsearchTemplate helper class that increases productivity performing common ES operations
    -  Annotation based mapping 
    -  Automatic implementation of Repository interfaces including support for custom finder methods
    -  Uses the Transport Client internally (at least for now, they will probably have to switch to another client)
    - drawbacks: can be a little behind the latest ES version, won't suit you if you prefer to communicate with the cluster over HTTP


### Dependencies
- [Elasticsearch Core Project](https://mvnrepository.com/artifact/org.elasticsearch/elasticsearch) 
- [Java High Level REST Client](https://mvnrepository.com/artifact/org.elasticsearch.client/elasticsearch-rest-high-level-client)
- [Java Low Level REST Client](https://mvnrepository.com/artifact/org.elasticsearch.client/elasticsearch-rest-client)
- [Gson](https://mvnrepository.com/artifact/com.google.code.gson/gson) (Optionally)

### Basic configuration

1. The properties for the ES Cluster needed to create the clients can be defined in applications.properties/application.yml:  
```java
elasticsearch:
    host: 54.15.56.111
    port: 9200
    cluster-name: DemoCluster
    auth:
        username: elastic
        password: 
```
`Note` If you are running Elasticsearch locally, the host should be *localhost*

2. Both the High Level and Low Level clients has to be defined as Spring Beans:  
```java
@Configuration
public class ElasticsearchConfig {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Value("${elasticsearch.host}")
    private String host;

    @Value("${elasticsearch.port}")
    private int port;

    @Value("${elasticsearch.cluster-name}")
    private String clusterName;

    @Value("${elasticsearch.auth.username}")
    private String username;

    @Value("${elasticsearch.auth.password}")
    private String password;

    @Bean
    public RestHighLevelClient highLevelClient() {
        RestClientBuilder builder = RestClient.builder(new HttpHost(host, port, "http"));
        Header[] defaultHeaders = new Header[] { new BasicHeader(AUTHORIZATION_HEADER, "Basic " + getAuthToken()) };
        builder.setDefaultHeaders(defaultHeaders);
        return new RestHighLevelClient(builder);
    }

    @Bean
    public RestClient lowLevelClient() {
        RestClientBuilder builder = RestClient.builder(new HttpHost(host, port, "http"));
        Header[] defaultHeaders = new Header[]{new BasicHeader(AUTHORIZATION_HEADER, "Basic " + getAuthToken())};
        builder.setDefaultHeaders(defaultHeaders);
        return builder.build();
    }

    private String getAuthToken() {
        String usernamePass = this.username + ":" + password;
        return new String(Base64.getEncoder().encode(usernamePass.getBytes()));
    }
}
```
`Note` We put the Authorization Header because we added some security on the ES Cluster with X-Pack, you are not required to do so.  

# Creating and deploying Elasticsearch Cluster (with Kibana and Logstash) on Azure

# Additional: Deploying you Java app on Azure
1. Since we are using IntelliJ, the most simple way is with the Azure plugin for IntelliJ (File > Settings > Plugins):
![](./images/intellij_azure_plugin.png)
2. Build a JAR version of your app
```
./mavenw build
```
3. Start the deployment with the azure plugin  
![](./images/azure-run-on-web-app.png)  
3. Select your Web App preferences (name, plan, resource group, etc.)  
![](./images/azure-plugin-run.png)  
