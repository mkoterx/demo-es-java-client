package com.mkoterx.demoesjavaclient;

import com.google.gson.Gson;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class MyController {

    private static long counter = 0;

    private final RestHighLevelClient esClient;
    private final RestClient lowLevelEsClient;
    private final Gson gson;

    public MyController(RestHighLevelClient esClient,
                        RestClient lowLevelEsClient,
                        Gson gson) {
        this.esClient = esClient;
        this.lowLevelEsClient = lowLevelEsClient;
        this.gson = gson;
    }

    @GetMapping(value = "test")
    public String getNumber() {
        return "Visit #" + counter++;
    }

    @PutMapping(value = "accounts")
    public Object createAccount(@RequestBody Account account) {
        IndexRequest request = new IndexRequest("test", "_doc");
        request.source(gson.toJson(account), XContentType.JSON);
        Object res;
        try {
            res = esClient.index(request).status();
        } catch (IOException e) {
            e.printStackTrace();
            res = e.getMessage();
        }
        return res;
    }

    @GetMapping(value = "accounts/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object search(@RequestParam(required = false) String extended, @RequestParam(required = false, defaultValue = "20") int size) {
        SearchRequest searchRequest = new SearchRequest("bank");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        String query =
                "{\n" +
                        "    \"bool\": {\n" +
                        "      \"filter\": {\n" +
                        "        \"range\": {\n" +
                        "          \"balance\": {\n" +
                        "            \"gte\": 20000,\n" +
                        "            \"lte\": 30000\n" +
                        "          }\n" +
                        "        }\n" +
                        "      }\n" +
                        "    }\n" +
                        "}";
        searchSourceBuilder.query(QueryBuilders.wrapperQuery(query));
        searchSourceBuilder.size(size);
//        searchSourceBuilder.aggregation(AggregationBuilders.)
//        searchSourceBuilder.sort("balance", SortOrder.DESC);

        searchRequest.source(searchSourceBuilder);
        try {
            SearchResponse searchResponse = esClient.search(searchRequest);
            if (extended != null) {
                return searchResponse.getHits().getHits();
            }
            return Arrays.stream(searchResponse.getHits().getHits())
                    .map(SearchHit::getSourceAsMap)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Trying the Low-Level Rest Client
    @GetMapping(value = "accounts/search-low", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object search() {
        String jsonString =
                "{\n" +
                " \"size\": 0,\n" +
                "  \"aggs\": {\n" +
                "    \"balance\": {\n" +
                "      \"range\":{\n" +
                "        \"field\": \"balance\",\n" +
                "        \"ranges\": [\n" +
                "          {\n" +
                "            \"from\": 1000,\n" +
                "            \"to\": 2000\n" +
                "          },\n" +
                "          {\n" +
                "            \"from\": 2000,\n" +
                "            \"to\": 3000\n" +
                "          }  \n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
        Request request = new Request("GET", "/bank/_search?pretty",   Collections.emptyMap(), new NStringEntity(jsonString, ContentType.APPLICATION_JSON));
        try {
            Response response = lowLevelEsClient.performRequest(request.getMethod(), request.getEndpoint(), request.getParameters(), request.getEntity());
            InputStreamReader reader = new InputStreamReader(response.getEntity().getContent());
            StringBuilder builder = new StringBuilder();
            int data = reader.read();
            while(data != -1){
                char c = (char) data;
                builder.append(c);
                data = reader.read();
            }
            reader.close();
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
