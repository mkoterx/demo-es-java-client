package com.mkoterx.demoesjavaclient;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Account {

    private long accountNumber;
    private long balance;
    private String firstName;
    private String lastName;
    private int age;
    private Gender gender;
    private String address;
    private String employer;
    private String email;
    private String city;
    private String state;

    public Account() {
    }

    public Account(long account_number,
                   long balance, String firstname,
                   String lastname,
                   int age,
                   Gender gender,
                   String address,
                   String employer,
                   String email,
                   String city,
                   String state) {
        this.accountNumber = account_number;
        this.balance = balance;
        this.firstName = firstname;
        this.lastName = lastname;
        this.age = age;
        this.gender = gender;
        this.address = address;
        this.employer = employer;
        this.email = email;
        this.city = city;
        this.state = state;
    }

    @JsonProperty("account_number")
    public long getAccountNumber() {
        return accountNumber;
    }

    public long getBalance() {
        return balance;
    }

    @JsonProperty("firstname")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("lastname")
    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getEmployer() {
        return employer;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }
}
