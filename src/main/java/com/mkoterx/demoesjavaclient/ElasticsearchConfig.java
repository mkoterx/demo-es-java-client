package com.mkoterx.demoesjavaclient;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Base64;

@Configuration
public class ElasticsearchConfig {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Value("${elasticsearch.host}")
    private String host;

    @Value("${elasticsearch.port}")
    private int port;

    @Value("${elasticsearch.cluster-name}")
    private String clusterName;

    @Value("${elasticsearch.auth.username}")
    private String username;

    @Value("${elasticsearch.auth.password}")
    private String password;

    @Bean
    public RestHighLevelClient highLevelClient() {
        RestClientBuilder builder = RestClient.builder(new HttpHost(host, port, "http"));
        Header[] defaultHeaders = new Header[] { new BasicHeader(AUTHORIZATION_HEADER, "Basic " + getAuthToken()) };
        builder.setDefaultHeaders(defaultHeaders);
        return new RestHighLevelClient(builder);
    }

    @Bean
    public RestClient lowLevelClient() {
        RestClientBuilder builder = RestClient.builder(new HttpHost(host, port, "http"));
        Header[] defaultHeaders = new Header[]{new BasicHeader(AUTHORIZATION_HEADER, "Basic " + getAuthToken())};
        builder.setDefaultHeaders(defaultHeaders);
        return builder.build();
    }

    private String getAuthToken() {
        String usernamePass = this.username + ":" + password;
        return new String(Base64.getEncoder().encode(usernamePass.getBytes()));
    }
}
